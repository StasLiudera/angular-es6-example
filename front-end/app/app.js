import 'babel-polyfill';

import '../scss/app.scss';

import angular from 'angular';
import uirouter from 'angular-ui-router';
import 'ngtouch';
import 'ng-storage';

import Router from './router';
import MainController from './controllers/main.controller';
import AppController from './controllers/app.controller';

const MODULE_NAME = 'angular-module';

angular.module(MODULE_NAME,
    [
        uirouter,
        'ngTouch',
        'ngStorage'
    ])
    .config(Router)
    .controller('MainController', MainController)
    .controller('AppController', AppController)
;

export default MODULE_NAME;
