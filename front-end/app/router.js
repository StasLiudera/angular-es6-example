export default function Router ($urlRouterProvider, $locationProvider, $stateProvider) {

    $stateProvider
        .state('app', {
            url: '/',
            views: {
                app: {
                    template: require('../templates/app.html'),
                    controller: 'AppController',
                    controllerAs: 'app'
                }
            }
        });

    $locationProvider.html5Mode(false);
    $urlRouterProvider.otherwise('/');
}

Router.$inject = ['$urlRouterProvider', '$locationProvider', '$stateProvider'];